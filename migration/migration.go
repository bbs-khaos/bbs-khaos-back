package migration

import (
	"github.com/jinzhu/gorm"
	"khao.fun/shizhan/model"
)

func SetAutoMigrate(db *gorm.DB) {
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.User_profile{}, &model.User_detail{})
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.Topic{}, &model.Topic_questions{}, &model.Topic_articles{})
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.Question_answers{})
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.Article_views{})
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.Comments{})
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.Messages{})
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.Image{})
	db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&model.File{})
}

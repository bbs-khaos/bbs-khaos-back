package main

import (
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"khao.fun/shizhan/common"
	_ "khao.fun/shizhan/docs"
	"khao.fun/shizhan/routes"
)

// @title Swagger Example API
// @version 1.0
// @description bbs-khaos-back
// @termsOfService https://khaos.fun

// @contact.name khaos
// @contact.url https://khaos.fun
// @contact.email 992308975@qq.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host 127.0.0.1:8081
// @BasePath /api/v1
func main() {
	InitConfig()
	port := viper.GetString("server.port")
	common.InitEmail()
	//common.Sentemail("992308975@qq.com","Email服务器开始测试","测试成功")
	db := common.InitDB()
	redis := common.InitRedis()
	defer redis.Close()
	defer db.Close()
	r := gin.Default()
	r = routes.CollectRoute(r)
	if port != "" {
		panic(r.Run(":" + port))
	}
	panic(r.Run())
}

func InitConfig() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir + "/config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

package controller

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/dto"
	"khao.fun/shizhan/model"
	"khao.fun/shizhan/response"
	"khao.fun/shizhan/service"
)

// @Summary 创建comment
// @Description 创建comment
// @Tags topic_comment
// @Accept mpfd
// @Produce json
// @Param to formData string true "to_user_id"
// @Param at formData string true "comment的位置  //article_id / answer_id"
// @Param type formData string true "comment的类型  0:VIEW  1:ANSWER"
// @Param text formData string true "comment的内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error","您无此权限","user info is wrong","Content cant be empty"}"
// @Router /topic/comment/comment [post]
func Comment_Create(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	curr_user_id := common.Uint32ToA(user.User_id)
	to_user_id := ctx.PostForm("to")     //to_user_id
	at_id := ctx.PostForm("at")          //at_id   //article_id / answer_id
	comment_type := ctx.PostForm("type") //comment_id  0:VIEW  1:ANSWER
	content := ctx.PostForm("text")      //comment_content

	if content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}
	var comment model.Comments
	comment.From_user_id = common.AtoUin32(curr_user_id)
	comment.To_user_id = common.AtoUin32(to_user_id)
	comment.At_id = common.AtoUin32(at_id)
	comment.Comment_type = uint8(common.AtoUin32(comment_type))
	comment.Comment_content = content

	err := service.Comments_Create(&comment)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}

// @Summary 获取comment总数
// @Description 获取comment总数
// @Tags topic_comment
// @Accept mpfd
// @Produce json
// @Param id formData string true "comment的位置  //article_id / answer_id"
// @Param type formData string true "comment的类型  0:VIEW  1:ANSWER"
// @Success 200 {string} string 
// @Failure 400 {string} string
// @Router /topic/comment/count/:id/:type [get]
func Comments_GetCount(ctx *gin.Context) {
	at_id := ctx.Param("id")          //at_id   //article_id / answer_id
	comment_type := ctx.Param("type") //comment_id  0:VIEW  1:ANSWER
	count, err := service.Comments_GetCount(at_id, comment_type)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, "")
		return
	}
	response.Success(ctx, gin.H{
		"count": strconv.FormatUint(uint64(count), 10),
	}, "")
}


// @Summary 获取部分comment
// @Description 获取部分comment
// @Tags topic_comment
// @Accept mpfd
// @Produce json
// @Param aid formData string true "comment的位置  //article_id / answer_id"
// @Param type formData string true "comment的类型  0:VIEW  1:ANSWER"
// @Param beg formData string true "从哪一条comment开始"
// @Param n formData string true "一共获取几条comment"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "can't find at_id or comment_type"}"
// @Router /topic/comment/listsome [get]
func Comments_ListSome(ctx *gin.Context) {
	at_id := ctx.PostForm("aid")         //at_id   //article_id / answer_id
	comment_type := ctx.PostForm("type") //comment_id  0:VIEW  1:ANSWER
	start := ctx.PostForm("beg")         //begin
	amount := ctx.PostForm("n")          //amount
	var result []model.Comments
	var result_json []dto.CommentsDto
	service.Comments_ListSome(&result, at_id, comment_type, common.AtoUin32(start), common.AtoUin32(amount))
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Comments(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find at_id or comment_type")
	} else {
		response.Success(ctx, result_json, "")
	}
}



// @Summary 获取全部comment
// @Description 获取全部comment
// @Tags topic_comment
// @Accept mpfd
// @Produce json
// @Param aid formData string true "comment的位置  //article_id / answer_id"
// @Param type formData string true "comment的类型  0:VIEW  1:ANSWER"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "can't find at_id or comment_type"}"
// @Router /topic/comment/listall/:id/:type [get]
func Comments_ListAll(ctx *gin.Context) {
	at_id := ctx.Param("id")          //at_id   //article_id / answer_id
	comment_type := ctx.Param("type") //comment_id  0:VIEW  1:ANSWER
	var result []model.Comments
	var result_json []dto.CommentsDto
	service.Comments_ListAll(&result, at_id, comment_type)
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Comments(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find at_id or comment_type")
	} else {
		response.Success(ctx, result_json, "")
	}
}

// @Summary 更新comment
// @Description 更新comment
// @Tags topic_comment
// @Accept mpfd
// @Produce json
// @Param cid formData string true "comment的id"
// @Param text formData string true "要更新的内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","Content cant be empty","您无此权限"}"
// @Router /topic/comment/content [put]
func Comment_Update(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	comment_id := ctx.PostForm("cid") //comment_id   //article_id / answer_id
	content := ctx.PostForm("text")   //comment_content
	if content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}
	var comment model.Comments
	comment.Comment_id = common.AtoUin32(comment_id)
	comment.Comment_content = content

	var temp model.Comments
	service.Comments_ListOne(comment_id,&temp)

	if user.User_identity==0 && temp.From_user_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}


	err := service.Comments_Update(&comment)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}


// @Summary 删除comment
// @Description 删除comment
// @Tags topic_comment
// @Accept mpfd
// @Produce json
// @Param cid formData string true "comment的id"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","您无此权限"}"
// @Router /topic/comment/comment [delete]
func Comment_Delete(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	comment_id := ctx.PostForm("cid") //comment_id   //article_id / answer_id
	var comment model.Comments
	comment.Comment_id = common.AtoUin32(comment_id)

	var temp model.Comments
	service.Comments_ListOne(comment_id,&temp)

	if user.User_identity==0 && temp.From_user_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}

	err := service.Comments_Delete(&comment)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}

//21.10.14 更新swagger
package controller

import (
	"github.com/gin-gonic/gin"
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/dto"
	"khao.fun/shizhan/model"
	"khao.fun/shizhan/response"
	"khao.fun/shizhan/service"
	"net/http"
	"strconv"
)

// @Summary 创建topic
// @Description 创建topic
// @Tags topic
// @Accept mpfd
// @Produce json
// @Param topic_title formData string true "话题名"
// @Param topic_introduction formData string true "话题简介"
// @Param topic_logo formData string true "话题logo"
// @Success 200 {string} string "{"msg": "创建成功"}"
// @Failure 400 {string} string "{"msg": "创建失败"}"
// @Router /topic/topic [post]
func Topic_Create(ctx *gin.Context) {
	title := ctx.PostForm("topic_title")
	introduction := ctx.PostForm("topic_introduction")
	logo := ctx.PostForm("topic_logo")
	topic := model.Topic{
		Topic_title:  title,
		Topic_logo: logo,
		Topic_introduction: introduction,
	}
	service.Topic_Create(&topic)
	if(topic.Topic_id==0){
		response.Fail(ctx,400,nil,"创建失败")
	}else {
		response.Success(ctx, topic, "创建成功")
	}

}

// @Summary 删除topic
// @Description 删除topic
// @Tags topic
// @Accept mpfd
// @Produce json
// @Param who query string true "id"
// @Success 200 {string} string "{"msg": "删除成功"}"
// @Failure 400 {string} string "{"msg": "error"}"
// @Router /topic/delete [delete]
func Topic_Delete(ctx *gin.Context) {
	id := ctx.Param("id")
	err:=service.Topic_Delete(id)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	}else {
		response.Success(ctx, nil, "删除成功")
	}
}

// @Summary 获取topic列表
// @Description 获取topic列表
// @Tags topic
// @Accept mpfd
// @Produce json
// @Success 200 {string} string "{"msg": "获取成功"}"
// @Failure 400 {string} string "{"msg": "话题列表为空"}"
// @Router /topic/listall [get]
func Topic_ListAll(ctx *gin.Context) {
	var res []model.Topic
	var res_json []dto.TopicDto
	service.Topic_ListAll(&res)
	for _, v := range res {
		res_json = append(res_json, dto.Dto_Topic(v))
	}
	if len(res) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "话题列表为空")
	} else {
		response.Success(ctx, res_json, "获取成功")
	}

}

// @Summary 获取topic数量
// @Description 获取topic数量
// @Tags topic
// @Accept mpfd
// @Produce json
// @Success 200 {string} string "{"msg": "获取成功"}"
// @Failure 400 {string} string "{"msg": "error"}"
// @Router /topic/count [get]
func Topic_GetCount(ctx *gin.Context) {
	count, err := service.Topic_GetCount()
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest,nil, err.Error())
	} else {
		response.Success(ctx, gin.H{
			"count": strconv.FormatUint(uint64(count), 10),
		}, "获取成功")
	}
}
// @Summary 更新topic
// @Description 更新topic
// @Tags topic
// @Accept mpfd
// @Produce json
// @Param topic_id formData string true "话题id"
// @Param topic_logo formData string true "话题logo"
// @Success 200 {string} string "{"msg": "更新成功"}"
// @Failure 400 {string} string "{"msg": "error"}"
// @Router /topic/logo [put]
func Topic_Update(ctx *gin.Context) {

	topic_id := ctx.PostForm("topic_id")
	topic_logo := ctx.PostForm("topic_logo")
	var data model.Topic
	data.Topic_id = common.AtoUin32(topic_id)
	data.Topic_logo = topic_logo
	if topic_logo == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Logo cant be empty")
		return
	}
	err := service.Topic_Update(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "更新成功")
	}
}

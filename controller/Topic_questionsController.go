package controller

import (
	"github.com/gin-gonic/gin"
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/dto"
	"khao.fun/shizhan/model"
	"khao.fun/shizhan/response"
	"khao.fun/shizhan/service"
	"net/http"
	"strconv"
)


// @Summary 创建topic下的question
// @Description 创建topic下的question
// @Tags topic_questions
// @Accept mpfd
// @Produce json
// @Param Topic_id formData string true "话题id"
// @Param Question_title formData string true "问题标题"
// @Param Question_content formData string true "问题内容"
// @Success 200 {string} string "{"msg": "创建成功"}"
// @Failure 400 {string} string "{"msg": "创建失败", "user info is wrong"}"
// @Router /topic/question/question [post]
func Topic_question_Create(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	curr_user_id := user.User_id
	Topic_id := ctx.PostForm("Topic_id")
	Question_pub_user_id := curr_user_id
	Question_title := ctx.PostForm("Question_title")
	Question_content := ctx.PostForm("Question_content")

	amount, _ := strconv.ParseUint(Topic_id, 10, 32)
	topic_id := uint32(amount)

	topic_questions := model.Topic_questions{
		Topic_id:                topic_id,
		Question_pub_user_id:    Question_pub_user_id,
		Question_title:          Question_title,
		Question_content:        Question_content,
		Question_answers_amount: 0,
	}

	err:=service.Topic_question_Create(&topic_questions)
	if err != nil {
		response.Fail(ctx, 400, nil, "创建失败")
	}else{
	response.Success(ctx, topic_questions, "创建成功")
}}


// @Summary 删除问题
// @Description 删除问题
// @Tags topic_questions
// @Accept mpfd
// @Produce json
// @Param id formData string true "问题id"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error", "user info is wrong","您无此权限"}"
// @Router /topic/question/question/:id [delete]
func Topic_question_Delete(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	curr_user_id := user.User_id
	question_id := ctx.Param("id")
	var data model.Topic_questions
	data.Question_id = common.AtoUin32(question_id)
	data.Question_pub_user_id = curr_user_id

	var temp model.Topic_questions
	service.Topic_question_ListOne(question_id,&temp)

	if user.User_identity==0 && temp.Question_pub_user_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}

	err := service.Topic_question_Delete(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}

// @Summary 获取所有问题
// @Description 获取所有问题
// @Tags topic_questions
// @Accept mpfd
// @Produce json
// @Param id formData string true "topic的id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error","can't find question_id"}"
// @Router /topic/question/listall/:id [get]
func Topic_question_ListAll(ctx *gin.Context) {
	id := ctx.Param("id")
	var result []model.Topic_questions
	var result_json []dto.Topic_questionsDto
	err:=service.Topic_question_ListAll(&result,id)

	if err != nil {
		response.Fail(ctx, http.StatusNotFound, nil, err.Error())
		return
	}
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Topic_questions(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find question_id")
	} else {
		response.Success(ctx, result_json, "")
	}
}


// @Summary 获取问题的总数
// @Description 获取问题的总数
// @Tags topic_questions
// @Accept mpfd
// @Produce json
// @Param id formData string true "topic的id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error"}"
// @Router /topic/question/count/:id [get]
func Topic_question_GetCount(ctx *gin.Context) {
	topic_id := ctx.Param("id")
	count, err := service.Topic_question_GetCount(topic_id)
	if err != nil {
		response.Success(ctx, gin.H{
			"count": strconv.FormatUint(uint64(count), 10),
		}, err.Error())
	} else {
		response.Success(ctx, gin.H{
			"count": strconv.FormatUint(uint64(count), 10),
		}, "")
	}

}

// @Summary 更新问题
// @Description 更新问题
// @Tags topic_questions
// @Accept mpfd
// @Produce json
// @Param question_id formData string true "问题的id"
// @Param content formData string true "更新的内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error","您无此权限","user info is wrong","Content cant be empty"}"
// @Router /topic/question/content [put]
func Topic_question_Update(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	curr_user_id := user.User_id
	question_id := ctx.PostForm("question_id")
	content := ctx.PostForm("question_content")


	var data model.Topic_questions
	data.Question_id = common.AtoUin32(question_id)
	data.Question_content = content
	data.Question_pub_user_id = curr_user_id


	var temp model.Topic_questions
	service.Topic_question_ListOne(question_id,&temp)

	if user.User_identity==0 && temp.Question_pub_user_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}

	if content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}
	err := service.Topic_question_Update(&data)

	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}

//21.10.14 更新swagger
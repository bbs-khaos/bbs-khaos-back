package controller

import (
	"github.com/gin-gonic/gin"
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/dto"
	"khao.fun/shizhan/model"
	"khao.fun/shizhan/response"
	"khao.fun/shizhan/service"
	"net/http"
	"strconv"
)

// @Summary 创建topic下的article
// @Description 创建topic下的article
// @Tags topic_articles
// @Accept mpfd
// @Produce json
// @Param Topic_id formData string true "话题id"
// @Param article_title formData string true "文章标题"
// @Param article_content formData string true "文章内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error","user info is wrong"}"
// @Router /topic/article/article [post]
func Topic_articles_Create(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	curr_user_id := user.User_id
	
	Topic_id := ctx.PostForm("Topic_id")
	Article_pub_user_id := curr_user_id
	Article_title := ctx.PostForm("article_title")
	Article_content := ctx.PostForm("article_content")

	amount,_:=strconv.ParseUint(Topic_id,10,32)
	topic_id :=uint32(amount)


	topic_articles := model.Topic_articles{
		Topic_id:  topic_id,
		Article_pub_user_id: Article_pub_user_id,
		Article_title:Article_title,
		Article_content:Article_content,
		Article_comments_amount:0,
	}

	err:=service.Topic_articles_Create(&topic_articles)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, topic_articles, "OK")
	}
}

// @Summary 删除topic下的article
// @Description 删除topic下的article
// @Tags topic_articles
// @Accept mpfd
// @Produce json
// @Param id formData string true "文章id"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","您无此权限"}"
// @Router /topic/article/article/:id [delete]
func Topic_articles_Delete(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}

	articles_id := ctx.Param("id")

	var data model.Topic_articles
	data.Article_id = common.AtoUin32(articles_id)

	var temp model.Topic_articles
	service.Topic_articles_ListOne(articles_id,&temp)

	if user.User_identity==0 && temp.Article_pub_user_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}


	err := service.Topic_articles_Delete(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}

}


// @Summary 获取topic下的article列表
// @Description 获取topic下的article列表
// @Tags topic_articles
// @Accept mpfd
// @Produce json
// @Param id formData string true "topic的id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "can't find articles_id"}"
// @Router /topic/article/listall/:id [get]
func Topic_articles_ListAll(ctx *gin.Context) {
	topic_id := ctx.Param("id")
	var result []model.Topic_articles
	var result_json []dto.Topic_articlesDto
	service.Topic_articles_ListAll(&result, topic_id)
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Topic_articles(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find articles_id")
	} else {
		response.Success(ctx, result_json, "")
	}
}

// @Summary 更新topic下的article
// @Description 更新topic下的article
// @Tags topic_articles
// @Accept mpfd
// @Produce json
// @Param article_id formData string true "文章的id"
// @Param article_content formData string true "文章要更新的内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "Content cant be empty","您无此权限","user info is wrong"}"
// @Router /topic/article/content [put]
func Topic_articles_Update(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}


	Article_id := ctx.PostForm("article_id")
	content := ctx.PostForm("article_content")
	var data model.Topic_articles
	data.Article_id = common.AtoUin32(Article_id)
	data.Article_content = content

	if content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}

	var temp model.Topic_articles
	service.Topic_articles_ListOne(Article_id,&temp)

	if user.User_identity==0 && temp.Article_pub_user_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}

	err := service.Topic_articles_Update(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}


// @Summary 获取topic下的article数量
// @Description 获取topic下的article数量
// @Tags topic_articles
// @Accept mpfd
// @Produce json
// @Param id formData string true "topic的id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error"}"
// @Router /topic/article/count/:id [get]
func Topic_articles_GetCount(ctx *gin.Context) {
	topic_id := ctx.Param("id")
	count, err := service.Topic_articles_GetCount(topic_id)
	if err != nil {
		response.Fail(ctx,http.StatusBadRequest,nil , err.Error())
	} else {
		response.Success(ctx, gin.H{
			"count": strconv.FormatUint(uint64(count), 10),
		}, "")
	}
}

//21.10.13更新swagger
package controller

import (
	"github.com/gin-gonic/gin"
	"khao.fun/shizhan/model"
	"khao.fun/shizhan/response"
	"khao.fun/shizhan/service"
	"strconv"
	"net/http"
	"strings"
)


// @Summary 获取用户关注、被关注、发布的问题、回答以及文章id
// @Description 获取用户关注、被关注、发布的问题、回答以及文章id
// @Tags User_follow
// @Produce json
// @Param userID query string true "用户ID"
// @Success 200 {string} string "{"msg": "获取成功"}"
// @Failure 400 {string} string "{"msg": "获取失败"}"
// @Router /user/detail [get]
func User_follow_detail(ctx *gin.Context) {
	userId := ctx.Query("userId")
	var user_detail model.User_detail
	user_id, _ := strconv.ParseUint(userId, 10, 32)
	service.User_Detail(uint32(user_id), &user_detail)
	if user_detail.User_id == 0 {
		response.Fail(ctx, 400, nil, "获取失败")
		return
	}
	response.Success(ctx, user_detail, "获取成功")
}

// @Summary 获取用户关注了哪些id
// @Description 获取用户关注了哪些id
// @Tags User_follow
// @Produce json
// @Param userID query string true "用户ID"
// @Success 200 {string} string "{"msg": "获取成功"}"
// @Failure 400 {string} string "{"msg": "获取失败"}"
// @Router /user/follows [get]
func User_follow_follows(ctx *gin.Context) {
	userId := ctx.Query("userId")
	var user_detail model.User_detail
	user_id,_:=strconv.Atoi(userId)
	service.User_Detail(uint32(user_id),&user_detail)
	if user_detail.User_id == 0 {
		response.Fail(ctx, 400, nil, "获取失败")
		return
	}
	follows:=user_detail.User_follows_id
	response.Success(ctx,follows,"获取成功")
}


// @Summary 获取哪些id关注了用户
// @Description 获取哪些id关注了用户
// @Tags User_follow
// @Produce json
// @Param userID query string true "用户ID"
// @Success 200 {string} string "{"msg": "获取成功"}"
// @Failure 400 {string} string "{"msg": "获取失败"}"
// @Router /user/followers [get]
func User_follow_followers(ctx *gin.Context) {
	userId := ctx.Query("userId")
	var user_detail model.User_detail
	user_id,_:=strconv.Atoi(userId)
	service.User_Detail(uint32(user_id),&user_detail)
	if user_detail.User_id == 0 {
		response.Fail(ctx, 400, nil, "获取失败")
		return
	}
	followers:=user_detail.User_followers_id
	response.Success(ctx,followers,"获取成功")
}

// @Summary 关注目标用户
// @Description 关注目标用户
// @Tags User_follow
// @Produce json
// @Param tId formData string true "被关注的用户ID"
// @Success 200 {string} string "{"msg": "关注成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","更新关注列表时失败","更新被关注列表时失败","目标用户不存在"}"
// @Router /user/follow [put]
func User_follow_follow(ctx *gin.Context){      //需要同时从关注者的列表里 和被关注者的列表里 添加对应信息
	user, ok := ctx.MustGet("user").(model.User_profile) //从ctx中获取关注者信息
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	var follower,followTo model.User_detail
	followToId,_:=strconv.Atoi(ctx.PostForm("tId"))  //从post中获取被关注者id
	service.User_Detail(uint32(followToId),&followTo)	 //查询被关注者信息
	if followTo.User_id == 0 {
		response.Fail(ctx, 400, nil, "目标用户不存在")
		return
	}
	// if int(user.User_id)==followToId{
	// 	response.Fail(ctx, http.StatusBadRequest, nil, "不能关注自己")
	// 	return
	// }
	service.User_Detail(uint32(user.User_id),&follower)  //查询关注者信息
	followInfo:=follower.User_follows_id 				 //获取关注者的关注id列表,string类型， 格式： (id)(id)
	followedInfo:=followTo.User_followers_id 			 //获取关注了被关注者的id列表，string类型， 格式： (id)(id)
	if strings.Contains(followInfo,"("+strconv.Itoa(followToId)+")"){
		response.Success(ctx,nil,"无法重复关注")
		return
	}
	followInfo=followInfo+"("+strconv.Itoa(followToId)+")"
	followedInfo=followedInfo+"("+strconv.Itoa(int(user.User_id))+")"
	err:=service.User_Follow_Update(uint32(user.User_id),followInfo)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新关注列表时失败")
		return
	}
	err=service.User_Follower_Update(uint32(followToId),followedInfo)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新被关注列表时失败")
		return
	}
	response.Success(ctx,nil,"关注成功")
}

// @Summary 取消关注目标用户
// @Description 取消关注目标用户
// @Tags User_follow
// @Produce json
// @Param uId formData string true "被取消关注的用户ID"
// @Success 200 {string} string "{"msg": "取消关注成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","对象用户错误","更新关注列表时失败","更新被关注列表时失败"}"
// @Router /user/unfollow [put]
func User_follow_unfollow(ctx *gin.Context){ 	 //需要同时从关注者的列表里 和被关注者的列表里 删除对应信息
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取关注者信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	unFollowId,_:=strconv.Atoi(ctx.PostForm("uId")) //从post中获取被取消关注者的id
	if unFollowId==0{
		response.Fail(ctx, http.StatusBadRequest, nil, "对象用户错误")
		return
	}
	var follower,followTo model.User_detail
	service.User_Detail(uint32(user.User_id),&follower)   //查询关注者信息
	service.User_Detail(uint32(unFollowId),&followTo)	//查询被关注者信息
	followInfo:=follower.User_follows_id  //获取关注者的关注id列表,string类型， 格式： (id)(id)
	followedInfo:=followTo.User_followers_id //获取关注了被关注者的id列表，string类型， 格式： (id)(id)
	followInfo=strings.Replace(followInfo,"("+strconv.Itoa(unFollowId)+")","",1)
	followedInfo=strings.Replace(followedInfo,"("+strconv.Itoa(int(user.User_id))+")","",1)
	err:=service.User_Follow_Update(uint32(user.User_id),followInfo)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新关注列表时失败")
		return
	}
	err=service.User_Follower_Update(uint32(unFollowId),followedInfo)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新被关注列表时失败")
		return
	}
	response.Success(ctx,nil,"取消关注成功")
}


// @Summary 向user_detail中记录新创建的question
// @Description 向user_detail中记录新创建的question
// @Tags User_follow
// @Produce json
// @Param qId formData string true "新创建的question的id"
// @Success 200 {string} string "{"msg": "创建问题成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","获取问题id时发生错误","更新用户问题列表时失败"}"
// @Router /user/newQuestion [put]
func User_follow_newQuestion(ctx *gin.Context){
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取用户信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	questionId,_:=strconv.Atoi(ctx.PostForm("qId")) //从post中获取questionId
	if questionId==0{
		response.Fail(ctx,http.StatusBadRequest,nil,"获取问题id时发生错误")
		return
	}
	var userDetail model.User_detail
	service.User_Detail(uint32(user.User_id),&userDetail) //查询用户信息
	questionList:=userDetail.User_questions_id   //获取用户发布的问题id，string类型， 格式： (id)(id)
	questionList=questionList+"("+strconv.Itoa(questionId)+")"
	err:=service.User_Question_Update(uint32(user.User_id),questionList)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户问题列表时失败")
		return
	}
	response.Success(ctx,nil,"创建问题成功")
}


// @Summary 从user_detail中移除新删除的question
// @Description 从user_detail中移除新删除的question
// @Tags User_follow
// @Produce json
// @Param qId formData string true "要删除的question的id"
// @Success 200 {string} string "{"msg": "删除问题成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","获取问题id时发生错误","更新用户问题列表时失败"}"
// @Router /user/deleteQuestion [put]
func User_follow_deleteQuestion(ctx *gin.Context){
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取用户信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	questionId,_:=strconv.Atoi(ctx.PostForm("qId")) //从post中获取questionId
	if questionId==0{
		response.Fail(ctx,http.StatusBadRequest,nil,"获取问题id时发生错误")
		return
	}
	var userDetail model.User_detail
	service.User_Detail(uint32(user.User_id),&userDetail) //查询用户信息
	questionList:=userDetail.User_questions_id   //获取用户发布的问题id，string类型， 格式： (id)(id)
	questionList=strings.Replace(questionList,"("+strconv.Itoa(questionId)+")","",1)
	err:=service.User_Question_Update(uint32(user.User_id),questionList)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户问题列表时失败")
		return
	}
	response.Success(ctx,nil,"删除问题成功")
}


// @Summary 向user_detail中记录新创建的answer
// @Description 向user_detail中记录新创建的answer
// @Tags User_follow
// @Produce json
// @Param qId formData string true "要创建的question的id"
// @Success 200 {string} string "{"msg": "创建回答成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","获取回答id时发生错误","更新用户回答列表时失败"}"
// @Router /user/newAnswer [put]
func User_follow_newAnswer(ctx *gin.Context){
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取用户信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	answerId,_:=strconv.Atoi(ctx.PostForm("aId")) //从post中获取answerId
	if answerId==0{
		response.Fail(ctx,http.StatusBadRequest,nil,"获取回答id时发生错误")
		return
	}
	var userDetail model.User_detail
	service.User_Detail(uint32(user.User_id),&userDetail) //查询用户信息
	answerList:=userDetail.User_answers_id   //获取用户发布的问题id，string类型， 格式： (id)(id)
	answerList=answerList+"("+strconv.Itoa(answerId)+")"
	err:=service.User_Answer_Update(uint32(user.User_id),answerList)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户回答列表时失败")
		return
	}
	response.Success(ctx,nil,"创建回答成功")
}



// @Summary 从user_detail中移除新删除地的answer
// @Description 从user_detail中移除新删除地的answer
// @Tags User_follow
// @Produce json
// @Param qId formData string true "要创建的question的id"
// @Success 200 {string} string "{"msg": "删除回答成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","获取回答id时发生错误","更新用户回答列表时失败"}"
// @Router /user/deleteAnswer [put]
func User_follow_deleteAnswer(ctx *gin.Context) {
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取用户信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	answerId,_:=strconv.Atoi(ctx.PostForm("aId")) //从post中获取answerId
	if answerId==0{
		response.Fail(ctx,http.StatusBadRequest,nil,"获取回答id时发生错误")
		return
	}
	var userDetail model.User_detail
	service.User_Detail(uint32(user.User_id),&userDetail) //查询用户信息
	answerList:=userDetail.User_answers_id   //获取用户发布的问题id，string类型， 格式： (id)(id)
	answerList=strings.Replace(answerList,"("+strconv.Itoa(answerId)+")","",1)
	err:=service.User_Question_Update(uint32(user.User_id),answerList)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户回答列表时失败")
		return
	}
	response.Success(ctx,nil,"删除回答成功")
}


// @Summary 向user_detail中记录新创建的article
// @Description 向user_detail中记录新创建的article
// @Tags User_follow
// @Produce json
// @Param qId formData string true "要创建的article的id"
// @Success 200 {string} string "{"msg": "创建文章成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","获取文章id时发生错误","更新用户文章列表时失败"}"
// @Router /user/newArticle [put]
func User_follow_newArticle(ctx *gin.Context){
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取用户信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	articleId,_:=strconv.Atoi(ctx.PostForm("aId")) //从post中获取articleId
	if articleId==0{
		response.Fail(ctx,http.StatusBadRequest,nil,"获取文章id时发生错误")
		return
	}
	var userDetail model.User_detail
	service.User_Detail(uint32(user.User_id),&userDetail) //查询用户信息
	articleList:=userDetail.User_articles_id   //获取用户发布的文章id，string类型， 格式： (id)(id)
	articleList=articleList+"("+strconv.Itoa(articleId)+")"
	err:=service.User_Article_Update(uint32(user.User_id),articleList)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户文章列表时失败")
		return
	}
	response.Success(ctx,nil,"创建文章成功")
}


// @Summary 从user_detail中移除新删除的article
// @Description 从user_detail中移除新删除的article
// @Tags User_follow
// @Produce json
// @Param qId formData string true "要删除的article的id"
// @Success 200 {string} string "{"msg": "删除文章成功"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","获取文章id时发生错误","更新用户文章列表时失败"}"
// @Router /user/deleteArticle [put]
func User_follow_deleteArticle(ctx *gin.Context) {
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取用户信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	articleId,_:=strconv.Atoi(ctx.PostForm("aId")) //从post中获取articleId
	if articleId==0{
		response.Fail(ctx,http.StatusBadRequest,nil,"获取文章id时发生错误")
		return
	}
	var userDetail model.User_detail
	service.User_Detail(uint32(user.User_id),&userDetail) //查询用户信息
	articleList:=userDetail.User_articles_id   //获取用户发布的文章id，string类型， 格式： (id)(id)
	articleList=strings.Replace(articleList,"("+strconv.Itoa(articleId)+")","",1)
	err:=service.User_Question_Update(uint32(user.User_id),articleList)
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户文章列表时失败")
		return
	}
	response.Success(ctx,nil,"删除文章成功")
}


// @Summary 清除用户所有关注与被关注信息(单向)
// @Description 清除用户所有关注与被关注信息(单向)--仅测试用
// @Tags User_follow
// @Produce json
// @Router /user/followReflesh [put]
func User_follow_reflesh(ctx *gin.Context) {
	user,ok:=ctx.MustGet("user").(model.User_profile) //从ctx中获取用户信息
	if !ok{
		response.Fail(ctx,http.StatusBadRequest,nil,"user info is wrong")
		return
	}
	err:=service.User_Follow_Update(uint32(user.User_id),"")
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户关注列表时失败")
		return
	}
	err=service.User_Follower_Update(uint32(user.User_id),"")
	if err!=nil{
		response.Fail(ctx, http.StatusBadRequest, nil, "更新用户被关注列表时失败")
		return
	}
	response.Success(ctx,nil,"清除成功")
}

//21.10.23创建
//注释掉的功能：防止自己关注自己(便于调试)
//待改进：关注的失败回滚、...
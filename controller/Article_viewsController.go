package controller

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/dto"
	"khao.fun/shizhan/model"
	"khao.fun/shizhan/response"
	"khao.fun/shizhan/service"
)


// @Summary 创建文章的评论
// @Description 创建文章的评论
// @Tags topic_articles_views
// @Accept mpfd
// @Produce json
// @Param aid formData string true "文章id"
// @Param text_title formData string true "评论内容"
// @Param article_content formData string true "文章内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error","user info is wrong", "Content cant be empty"}"
// @Router /topic/article/view/view [post]
func Article_Views_Create(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	article_id := ctx.PostForm("aid")    //article_id
	view_content := ctx.PostForm("text") //view_content
	if view_content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}
	var Article_view model.Article_views
	Article_view.Article_id = common.AtoUin32(article_id)
	Article_view.User_id = user.User_id
	Article_view.View_content = view_content
	err := service.Article_view_Create(&Article_view)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}


// @Summary 获取文章中评论的数量
// @Description 获取文章中评论的数量
// @Tags topic_articles_views
// @Accept mpfd
// @Produce json
// @Param id formData string true "文章id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error"}"
// @Router /topic/article/view/count/:id [get]
func Article_Views_GetCount(ctx *gin.Context) {
	article_id := ctx.Param("id") //article_id
	count, err := service.Article_views_GetCount(article_id)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, gin.H{
			"count": 0,
		}, err.Error())
	} else {
		response.Success(ctx, gin.H{
			"count": strconv.FormatUint(uint64(count), 10),
		}, "")
	}
}

// @Summary 获取文章中部分评论
// @Description 获取文章中部分评论
// @Tags topic_articles_views
// @Accept mpfd
// @Produce json
// @Param aid formData string true "文章id"
// @Param beg formData string true "从第几个评论开始"
// @Param n formData string true "一共获取几个评论"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error","can't find views"}"
// @Router /topic/article/view/listsome [get]
func Article_Views_ListSome(ctx *gin.Context) {
	article_id := ctx.PostForm("aid") //article_id
	start := ctx.PostForm("beg")      //begin
	amount := ctx.PostForm("n")       //amount
	var result []model.Article_views
	var result_json []dto.Article_viewsDto
	err := service.Article_views_ListSome(&result, article_id, common.AtoUin32(start), common.AtoUin32(amount))
	if err != nil {
		response.Fail(ctx, http.StatusNotFound, nil, err.Error())
		return
	}
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Article_views(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find views")
	} else {
		response.Success(ctx, result_json, "")
	}
}


// @Summary 获取文章中所有评论
// @Description 获取文章中所有评论
// @Tags topic_articles_views
// @Accept mpfd
// @Produce json
// @Param id formData string true "文章id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error","can't find views"}"
// @Router /topic/article/view/listall/:id [get]
func Article_Views_ListAll(ctx *gin.Context) {
	article_id := ctx.Param("id")
	var result []model.Article_views
	var result_json []dto.Article_viewsDto
	err := service.Article_views_ListAll(&result, article_id)
	if err != nil {
		response.Fail(ctx, http.StatusNotFound, nil, err.Error())
		return
	}
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Article_views(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find views")
	} else {
		response.Success(ctx, result_json, "")
	}
}

// @Summary 更新评论
// @Description 更新评论
// @Tags topic_articles_views
// @Accept mpfd
// @Produce json
// @Param vid formData string true "评论id"
// @Param text formData string true "评论更新的内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error","user info is wrong","Content cant be empty","您无此权限"}"
// @Router /topic/article/view/content [put]
func Article_View_Update(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}


	view_id := ctx.PostForm("vid")  //view_id
	content := ctx.PostForm("text") //view_content
	var data model.Article_views
	data.View_id = common.AtoUin32(view_id)
	data.View_content = content
	if content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}

	var temp model.Article_views
	service.Article_view_ListOne(view_id,&temp)

	if user.User_identity==0 && temp.User_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}


	err := service.Article_view_Update(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}


// @Summary 删除评论
// @Description 删除评论
// @Tags topic_articles_views
// @Accept mpfd
// @Produce json
// @Param vid formData string true "评论id"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error","user info is wrong","您无此权限"}"
// @Router /topic/article/view/view [delete]
func Article_view_Delete(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}


	view_id := ctx.PostForm("vid") //view_id
	var data model.Article_views
	data.View_id = common.AtoUin32(view_id)

	var temp model.Article_views
	service.Article_view_ListOne(view_id,&temp)

	if user.User_identity==0 && temp.User_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}

	err := service.Article_view_Delete(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}

//21.10.14更新swagger 
package controller

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/dto"
	"khao.fun/shizhan/model"
	"khao.fun/shizhan/response"
	"khao.fun/shizhan/service"
)


// @Summary 创建问题的回答
// @Description 创建问题的回答
// @Tags topic_question_answer
// @Accept mpfd
// @Produce json
// @Param qid formData string true "问题的id"
// @Param ctx formData string true "回答的内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "user info is wrong","Content cant be empty"}"
// @Router /topic/question/answer/answer [post]
func Question_Answers_Create(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}

	question_id := ctx.PostForm("qid")    //question_id
	answer_content := ctx.PostForm("ctx") //answer_content
	if answer_content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}
	var Question_answers model.Question_answers
	Question_answers.Question_id = common.AtoUin32(question_id)
	Question_answers.User_id = user.User_id
	Question_answers.Answer_content = answer_content
	err := service.Question_answer_Create(&Question_answers)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}


// @Summary 获取问题的回答总数
// @Description 获取问题的回答总数
// @Tags topic_question_answer
// @Accept mpfd
// @Produce json
// @Param id formData string true "问题的id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error"}"
// @Router /topic/question/answer/count/:id [get]
func Question_Answers_GetCount(ctx *gin.Context) {
	question_id := ctx.Param("id") //question_id
	count, err := service.Question_answer_GetCount(question_id)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, gin.H{
			"count": 0,
		}, err.Error())
	} else {
		response.Success(ctx, gin.H{
			"count": strconv.FormatUint(uint64(count), 10),
		}, "")
	}
}


// @Summary 获取问题的部分回答
// @Description 获取问题的部分回答
// @Tags topic_question_answer
// @Accept mpfd
// @Produce json
// @Param qid formData string true "问题的id"
// @Param beg formData string true "从第几个回答开始"
// @Param n formData string true "获取几个回答"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error"，"can't find answers"}"
// @Router /topic/question/answer/listsome [get]
func Question_Answers_ListSome(ctx *gin.Context) {
	question_id := ctx.PostForm("qid") //question_id
	start := ctx.PostForm("beg")       //begin
	amount := ctx.PostForm("n")        //amount
	var result []model.Question_answers
	var result_json []dto.Question_answersDto
	err := service.Question_answer_ListSome(&result, question_id, common.AtoUin32(start), common.AtoUin32(amount))
	if err != nil {
		response.Fail(ctx, http.StatusNotFound, nil, err.Error())
		return
	}
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Question_answers(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find answers")
	} else {
		response.Success(ctx, result_json, "")
	}
}


// @Summary 获取问题的全部回答
// @Description 获取问题的全部回答
// @Tags topic_question_answer
// @Accept mpfd
// @Produce json
// @Param id formData string true "问题的id"
// @Success 200 {string} string 
// @Failure 400 {string} string "{"msg": "error"，"can't find answers"}"
// @Router /topic/question/answer/listall/:id [get]
func Question_Answers_ListAll(ctx *gin.Context) {
	question_id := ctx.Param("id") //question_id
	var result []model.Question_answers
	var result_json []dto.Question_answersDto
	err := service.Question_answer_ListAll(&result, question_id)
	if err != nil {
		response.Fail(ctx, http.StatusNotFound, nil, err.Error())
		return
	}
	for _, v := range result {
		result_json = append(result_json, dto.Dto_Question_answers(v))
	}
	if len(result) == 0 {
		response.Fail(ctx, http.StatusNotFound, nil, "can't find answers")
	} else {
		response.Success(ctx, result_json, "")
	}
}


// @Summary 更新回答
// @Description 更新回答
// @Tags topic_question_answer
// @Accept mpfd
// @Produce json
// @Param aid formData string true "回答的id"
// @Param text formData string true "更新的回答的内容"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error"，"user info is wrong", "Content cant be empty","您无此权限"}"
// @Router /topic/question/answer/content [put]
func Question_Answer_Update(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}
	curr_user_id := user.User_id

	answer_id := ctx.PostForm("aid") //answer_id
	content := ctx.PostForm("text")  //answer_content
	var data model.Question_answers
	data.Answer_id = common.AtoUin32(answer_id)
	data.Answer_content = content
	data.User_id = curr_user_id
	if content == "" {
		response.Fail(ctx, http.StatusBadRequest, nil, "Content cant be empty")
		return
	}

	var temp model.Question_answers
	service.Question_answer_ListOne(answer_id,&temp)

	if user.User_identity==0 && temp.User_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}

	err := service.Question_answer_Update(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}


// @Summary 删除回答
// @Description 删除回答
// @Tags topic_question_answer
// @Accept mpfd
// @Produce json
// @Param aid formData string true "回答的id"
// @Success 200 {string} string "{"msg": "OK"}"
// @Failure 400 {string} string "{"msg": "error"，"user info is wrong","您无此权限"}"
// @Router /topic/question/answer/answer [delete]
func Question_Answer_Delete(ctx *gin.Context) {
	user, ok := ctx.MustGet("user").(model.User_profile)
	if !ok {
		response.Fail(ctx, http.StatusBadRequest, nil, "user info is wrong")
		return
	}

	answer_id := ctx.PostForm("aid") //answer_id
	var data model.Question_answers
	data.Answer_id = common.AtoUin32(answer_id)

	var temp model.Question_answers
	service.Question_answer_ListOne(answer_id,&temp)

	if user.User_identity==0 && temp.User_id!=user.User_id {
		response.Fail(ctx, http.StatusBadRequest, nil, "您无此权限")
		return
	}

	err := service.Question_answer_Delete(&data)
	if err != nil {
		response.Fail(ctx, http.StatusBadRequest, nil, err.Error())
	} else {
		response.Success(ctx, nil, "OK")
	}
}

//21.10.14 更新sawwger
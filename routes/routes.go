package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"khao.fun/shizhan/controller"
	"khao.fun/shizhan/middleware"
)

func CollectRoute(r *gin.Engine) *gin.Engine {
	r.Use(Cors()) //默认跨域
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.StaticFS("upload_Images", http.Dir("./upload_Images")) //这样可以直接在前端查看到图片不用下载http://localhost:8081/upload_Images/2021/11/09/xEFsXLNOcNltmpzlqhZy.png
	v1 := r.Group("/api/v1")
	{
		auth := v1.Group("/auth")
		{
			auth.POST("register", controller.User_profile_Resgister)
			auth.POST("code", controller.User_profile_Code)
			auth.POST("login", controller.User_profile_Login)
			auth.GET("info", middleware.AuthMiddleware(middleware.AccLogined), controller.User_profile_Info)
			auth.DELETE("info", middleware.AuthMiddleware(middleware.AccSuper), controller.User_profile_Delete)
			auth.PUT("upgrade", middleware.AuthMiddleware(middleware.AccSuper), controller.User_profile_Upgrade)
		}
		topic := v1.Group("/topic")
		{
			topic.GET("listall", controller.Topic_ListAll)
			topic.GET("count", controller.Topic_GetCount)
			topic.POST("topic", middleware.AuthMiddleware(middleware.AccSuper), controller.Topic_Create)
			topic.DELETE("delete/:id", middleware.AuthMiddleware(middleware.AccSuper), controller.Topic_Delete)
			topic.PUT("logo", middleware.AuthMiddleware(middleware.AccSuper), controller.Topic_Update)

			question := topic.Group("/question")
			{
				question.GET("listall/:id", controller.Topic_question_ListAll)
				question.GET("count/:id", controller.Topic_question_GetCount)
				question.PUT("content", middleware.AuthMiddleware(middleware.AccLogined), controller.Topic_question_Update)
				question.POST("question", middleware.AuthMiddleware(middleware.AccLogined), controller.Topic_question_Create)
				question.DELETE("question/:id", middleware.AuthMiddleware(middleware.AccLogined), controller.Topic_question_Delete)
				answer := question.Group("/answer")
				{
					answer.GET("/count/:id", controller.Question_Answers_GetCount)
					answer.POST("/listsome", controller.Question_Answers_ListSome)
					answer.GET("/listall/:id", controller.Question_Answers_ListAll)
					answer.Use(middleware.AuthMiddleware(middleware.AccLogined)).POST("/answer", controller.Question_Answers_Create)
					answer.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("/content", controller.Question_Answer_Update)
					answer.Use(middleware.AuthMiddleware(middleware.AccLogined)).DELETE("/answer", controller.Question_Answer_Delete)
				}
			}
			article := topic.Group("/article")
			{
				article.GET("listall/:id", controller.Topic_articles_ListAll)
				article.GET("count/:id", controller.Topic_articles_GetCount)
				article.POST("article", middleware.AuthMiddleware(middleware.AccLogined), controller.Topic_articles_Create)
				article.PUT("content", middleware.AuthMiddleware(middleware.AccLogined), controller.Topic_articles_Update)
				article.DELETE("article/:id", middleware.AuthMiddleware(middleware.AccLogined), controller.Topic_articles_Delete)
				view := article.Group("/view")
				{
					view.GET("/count/:id", controller.Article_Views_GetCount)
					view.GET("/listsome", controller.Article_Views_ListSome)
					view.GET("/listall/:id", controller.Article_Views_ListAll)
					view.Use(middleware.AuthMiddleware(middleware.AccLogined)).POST("/view", controller.Article_Views_Create)
					view.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("/content", controller.Article_View_Update)
					view.Use(middleware.AuthMiddleware(middleware.AccLogined)).DELETE("/view", controller.Article_view_Delete)
				}
			}

			comment := topic.Group("/comment")
			{

				comment.GET("/count/:id/:type", controller.Comments_GetCount)
				comment.GET("/listsome", controller.Comments_ListSome)
				comment.GET("/listall/:id/:type", controller.Comments_ListAll)
				comment.Use(middleware.AuthMiddleware(middleware.AccLogined)).POST("comment", controller.Comment_Create)
				comment.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("content", controller.Comment_Update)
				comment.Use(middleware.AuthMiddleware(middleware.AccLogined)).DELETE("comment", controller.Comment_Delete)
			}

		}
		user := v1.Group("/user")
		{
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).GET("detail", controller.User_follow_detail)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).GET("follows", controller.User_follow_follows)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).GET("followers", controller.User_follow_followers)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("follow", controller.User_follow_follow)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("unfollow", controller.User_follow_unfollow)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("newQuestion", controller.User_follow_newQuestion)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("deleteQuestion", controller.User_follow_deleteQuestion)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("newAnswer", controller.User_follow_newAnswer)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("deleteAnswer", controller.User_follow_deleteAnswer)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("newArticle", controller.User_follow_newArticle)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("deleteArticle", controller.User_follow_deleteArticle)
			user.Use(middleware.AuthMiddleware(middleware.AccLogined)).PUT("followReflesh", controller.User_follow_reflesh)
		}
		upload := v1.Group("/upload")
		{
			upload.GET("imageList", controller.Image_ListAll)
			upload.GET("fileList", controller.File_ListAll)
			upload.Use(middleware.AuthMiddleware(middleware.AccLogined)).POST("image", controller.Upload_image)
			upload.Use(middleware.AuthMiddleware(middleware.AccLogined)).POST("file", controller.Upload_file)
			upload.Use(middleware.AuthMiddleware(middleware.AccLogined)).DELETE("deleteImage", controller.Delete_Image)
			upload.Use(middleware.AuthMiddleware(middleware.AccLogined)).DELETE("deleteFile", controller.Delete_File)
		}
		download := v1.Group("/download")
		{
			download.Use(middleware.AuthMiddleware(middleware.AccLogined)).GET("image", controller.DownloadImage)
			download.Use(middleware.AuthMiddleware(middleware.AccLogined)).GET("file", controller.DownloadFile)
		}

	}
	return r
}

func Cors() gin.HandlerFunc {

	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.Request.Header.Get("Origin")
		if origin != "" {
			c.Header("Access-Control-Allow-Origin", "*")
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization")
			c.Header("Access-Control-Allow-Credentials", "true")
			c.Set("content-type", "application/json")
		}
		//放行所有OPTIONS方法
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}

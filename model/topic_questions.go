package model

import "time"

type Topic_questions struct {
	Topic_id                uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'发布于那个话题'"`
	Question_id             uint32    `gorm:"primary_key;NOT NULL;COMMENT:'问题的ID'"`
	Question_pub_user_id    uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'问题发布者ID'"`
	Question_title          string    `gorm:"type:varchar(255);NOT NULL;COMMENT:'问题标题'"`
	Question_content        string    `gorm:"type:text;NOT NULL;COMMENT:'问题具体内容'"`
	Question_answers_amount uint32    `gorm:"type:int(11);DEFAULT:0;COMMENT:'回答数量,高频查询'"`
	Created_date            time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date            time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

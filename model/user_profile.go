package model

import (
	"time"
)

type User_profile struct {
	User_id           uint32    `gorm:"primary_key"`
	User_name         string    `gorm:"type:varchar(20);not null"`
	User_pwd          string    `gorm:"size:255;not null"`
	User_email        string    `gorm:"type:varchar(32);not null"`
	User_identity     uint8     `gorm:"type:tinyint(4);COMMENT:'0:User,1:Administrator,2:Super'"`
	User_tel          string    `gorm:"type:varchar(11)"`
	User_sex          string    `gorm:"type:varchar(11)"`
	User_logo         string    `gorm:"type:varchar(255)"`
	User_province     string    `gorm:"type:varchar(16)"`
	User_city         string    `gorm:"type:varchar(16)"`
	User_introduction string    `gorm:"type:varchar(255)"`
	Created_date      time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date      time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

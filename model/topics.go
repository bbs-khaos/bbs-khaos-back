package model

import (
	"time"
)

type Topic struct {
	Topic_id                   uint32    `gorm:"primary_key;NOT NULL;COMMENT:'话题ID'"`
	Topic_logo                 string    `gorm:"type:varchar(255);NOT NULL;COMMENT:'logo'"`
	Topic_title                string    `gorm:"type:varchar(255);NOT NULL;COMMENT:'话题标题'"`
	Topic_introduction         string    `gorm:"type:varchar(500);DEFAULT NULL;COMMENT:'话题简介'"`
	Topic_subscription_user_id string    `gorm:"type:text;COMMENT:'话题订阅者的用户ID，json格式'"`
	Created_date               time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date               time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

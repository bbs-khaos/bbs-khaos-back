package model

import (
	"time"
)

type Image struct {
	Id           int       `gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`      // 图片id
	Type         string    `gorm:"column:type;type:varchar(255);not null" sql:"index"` // 类型 (avatar,topic)
	UserId       uint32    `gorm:"column:user_id;not null" sql:"index"`                // 上传的用户 id
	ImagePath    string    `gorm:"column:path;type:varchar(255);not null"`             // 图片路径
	Created_date time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

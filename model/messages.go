package model

import "time"

type Messages struct {
	Message_id      uint32    `gorm:"primary_key;NOT NULL;COMMENT:'消息ID'"`
	Belong_user_id  uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'该消息是属于哪个USER的'"`
	Type            uint8     `gorm:"type:tinyint(4);NOT NULL;COMMENT:'消息类型0:COMMENT 1: LIKE 2:FOLLOW'"`
	Subject_user_id uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'产生这个消息的用户ID'"`
	Module_id       uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'具体的VIEW_ID/ANSWER_ID'"`
	Created_date    time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date    time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

package model

import (
	"time"
)

type User_detail struct {
	User_id           uint32    `gorm:"NOT NULL;COMMENT:'用户ID'"`
	User_follows_id   string    `gorm:"COMMENT:'用户关注的用户ID，json格式'"`
	User_followers_id string    `gorm:"COMMENT:'关注用户的用户ID，json格式'"`
	User_questions_id string    `gorm:"COMMENT:'用户发布的问题ID，json格式'"`
	User_answers_id   string    `gorm:"COMMENT:'用户回答的回答ID，json格式'"`
	User_articles_id  string    `gorm:"COMMENT:'用户发布的文章ID，json格式'"`
	Created_date      time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date      time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

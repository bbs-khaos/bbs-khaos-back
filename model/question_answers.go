package model

import "time"

type Question_answers struct {
	Question_id    uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'挂载的问题ID'"`
	Answer_id      uint32    `gorm:"primary_key;NOT NULL;COMMENT:'回答ID'"`
	User_id        uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'回答者ID'"`
	Answer_content string    `gorm:"type:text   ;NOT NULL;COMMENT:'回答正文'"`
	Answer_like    uint32    `gorm:"type:int(11);DEFAULT:'0';COMMENT:'支持人数'"`
	Answer_unlike  uint32    `gorm:"type:int(11);DEFAULT:'0';COMMENT:'反对人数'"`
	Created_date   time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date   time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

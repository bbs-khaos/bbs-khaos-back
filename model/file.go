package model

import (
	"time"
)

type File struct {
	Id           int       `gorm:"column:id;primary_key;AUTO_INCREMENT;not null"`          //文件id
	UserId       uint32    `gorm:"column:user_id;not null" sql:"index"`                    //上传的用户 id
	Filename     string    `gorm:"column:filename;type:varchar(255);not null" sql:"index"` //文件名
	FilePath     string    `gorm:"column:path;type:varchar(255);not null"`                 //文件路径
	Created_date time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

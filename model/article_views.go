package model

import "time"

type Article_views struct {
	Article_id   uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'挂载的文章ID'"`
	View_id      uint32    `gorm:"primary_key;NOT NULL;COMMENT:'评论的ID'"`
	User_id      uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'发表评论的人的ID'"`
	View_content string    `gorm:"type:text;COMMENT:'评论正文'"`
	View_like    uint32    `gorm:"type:int(11);DEFAULT:0;COMMENT:'评论支持人数'"`
	View_unlike  uint32    `gorm:"type:int(11);DEFAULT:0;COMMENT:'评论反对人数'"`
	Created_date time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

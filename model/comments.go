package model

import "time"

type Comments struct {
	Comment_id      uint32    `gorm:"primary_key;NOT NULL;COMMENT:'回复ID，不同于view_id'"`
	Comment_type    uint8     `gorm:"type:tinyint(4);NOT NULL;COMMENT:'该回复是属于0:VIEW  1:ANSWER'"`
	At_id           uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'挂载到VIEW_ID/ANSWER_ID'"`
	From_user_id    uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'发回复的人ID'"`
	To_user_id      uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'回复的对象默认应该是VIEW/ANSWER的发布者ID'"`
	Comment_content string    `gorm:"type:text;NOT NULL;COMMENT:'回复内容'"`
	Comment_like    uint32    `gorm:"type:int(11);DEFAULT:'0';COMMENT '支持人数'"`
	Comment_unlike  uint32    `gorm:"type:int(11);DEFAULT:'0';COMMENT '反对人数'"`
	Created_date    time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date    time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

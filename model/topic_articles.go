package model

import "time"

type Topic_articles struct {
	Topic_id                uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'话题ID'"`
	Article_id              uint32    `gorm:"primary_key;NOT NULL;COMMENT:'文章ID'"`
	Article_pub_user_id     uint32    `gorm:"type:int(11);NOT NULL;COMMENT:'文章发布者ID'"`
	Article_title           string    `gorm:"type:varchar(255);NOT NULL;COMMENT:'文章标题'"`
	Article_content         string    `gorm:"type:text;NOT NULL;COMMENT:'文章正文'"`
	Article_comments_amount uint32    `gorm:"type:int(11);DEFAULT:'0';COMMENT:'文章评论数量，高频查询'"`
	Created_date            time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP"`
	Updated_date            time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}

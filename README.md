# bbs-khaos-back

### 项目地址

前端：[bbs-khaos/bbs-khaos - Gitee.com](https://gitee.com/bbs-khaos/bbs-khaos/tree/master/)

后端：[bbs-khaos-back: Golang制作的bbs论坛 (gitee.com)](https://gitee.com/bbs-khaos/bbs-khaos-back)

交流群：731903347

### 项目介绍

`bbs-khaos`是一个使用Go语言搭建的开源论坛程序系统，采用前后端分离技术，Go语言提供api进行数据支撑，用户界面使用Vue/React/小程序进行渲染，后台界面基于element-ui-plus。如果你正在学习Go语言，或者考虑转Go语言的Phper/Javaer...那么该项目对你有的学习会有很大的帮助，欢迎一起来交流。

#### 软件架构
│  .gitignore
│  go.mod
│  go.sum
│  LICENSE
│  main.go
│  README.md
│  tree.md
│  
├─common
│      convert.go
│      database.go
│      email.go
│      jwt.go
│      redis.go
│      util.go
│      
├─config
│      application.yml
│      
├─controller
│      Article_viewsController.go
│      CommentsController.go
│      Download.go
│      Question_answerController.go
│      TopicController.go
│      Topic_articlesController.go
│      Topic_questionsController.go
│      Upload.go
│      User_followController.go
│      User_profileController.go
│      
├─docs
│      docs.go
│      swagger.json
│      swagger.yaml
│      
├─dto
│      article_views_dto.go
│      comments_dto.go
│      file_dto.go
│      image_dto.go
│      messages_dto.go
│      question_answers_dto.go
│      topics_dto.go
│      topic_articles_dto.go
│      topic_questions_dto.go
│      user_detail_dto.go
│      user_profile_dto.go
│      
├─middleware
│      AuthMiddleware.go
│      
├─migration
│      migration.go
│      
├─model
│      article_views.go
│      comments.go
│      file.go
│      image.go
│      messages.go
│      question_answers.go
│      topics.go
│      topic_articles.go
│      topic_questions.go
│      user_detail.go
│      user_profile.go
│      
├─response
│      response.go
│      
├─routes
│      routes.go
│      
└─service
        srv_article_views.go
        srv_comments.go
        srv_files.go
        srv_images.go
        srv_question_answers.go
        srv_topic.go
        srv_topic_articles.go
        srv_topic_question.go
        srv_user_follow.go
        srv_user_profile.go
        


#### 安装教程

1.  git clone https://gitee.com/bbs-khaos/bbs-khaos-back.git
2.  go mod tidy
3.  go run main.go

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


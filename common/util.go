package common

import (
	"math/rand"
	"os"
	"path/filepath"
	"regexp"
	"time"
)

func RandaomString(n int) string {
	var letters = []byte("dsadqwfsedgftewrwrsdfSGSFTJYIUPJMBVBNXBFjyuyuioupihkfghFSFHRTRGXCBCVJGHIYTUERTEWQW")
	result := make([]byte, n)
	rand.Seed(time.Now().Unix())
	for i := range result {
		result[i] = letters[rand.Intn(len(letters))]
	}
	return string(result)
}

func VerifyEmailFormat(email string) bool {
	pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(email)
}

//自定义一个创建文件目录的函数,并返回最终的文件夹路径
func Mkdir(basePath string) string {
	//	1.获取当前时间,并且格式化时间
	folderName := time.Now().Format("2006/01/02")
	folderPath := filepath.Join(basePath, folderName)
	//使用mkdirall会创建多层级目录
	os.MkdirAll(folderPath, os.ModePerm)
	return folderPath
}

//自定义一个创建随机文件名的函数，长度为20位，由大小写字母组成
func RandomName() string {
	name := make([]byte, 20)
	i := 0
	for i < 20 {
		//利用rand.Intn(x)来伪随机生成一个[0,x)的数
		a := rand.Intn(26)
		b := rand.Intn(2)
		if b == 1 {
			name[i] = byte('A' + a)
		} else {
			name[i] = byte('a' + a)
		}
		i++
	}
	return string(name)
}

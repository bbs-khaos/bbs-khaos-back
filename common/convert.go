package common

import "strconv"

func AtoUin32(a string) uint32 {
	t, _ := strconv.ParseUint(a, 10, 32)
	return uint32(t)
}

func AtoUin8(a string) uint8 {
	t, _ := strconv.ParseUint(a, 10, 32)
	return uint8(t)
}

func Uint32ToA(u uint32) string {
	return strconv.FormatUint(uint64(u), 10)
}

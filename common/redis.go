package common

import (
	"fmt"
	"strconv"
	"time"
)

func View(id uint32,name string) uint64{
	countStr,_:=RedisClient.Get(ViewKey(id,name)).Result()
	count,_:=strconv.ParseUint(countStr,10,64)
	return count
}
func AddView(id uint32,name string){
	RedisClient.Incr(ViewKey(id,name))
}

func ViewKey(id uint32,name string) string{
	return fmt.Sprintf("view:%s:%s",name,strconv.Itoa(int(id)))
}

func CodeKey(name string,email string) string{
	return fmt.Sprintf("code:%s:%s",email,name)
}

func CodeChange(name string,email string) string{
	code:=Code(name,email)
	if code!=""{
		return "-1"
	}
	icode:=RandaomString(4)
	RedisClient.Set(CodeKey(name,email),icode,100*time.Second)
	return icode
}

func Code(name string,email string) string{
	countStr,_:=RedisClient.Get(CodeKey(name,email)).Result()
	return countStr
}
package common

import (
	"log"
	"net/smtp"
	"github.com/spf13/viper"
	"github.com/jordan-wright/email"
)
var addr string
var username string
var host string
var password string
func InitEmail(){
	addr = viper.GetString("website.addr")
	username = viper.GetString("website.username")
	host = viper.GetString("website.host")
	password = viper.GetString("website.password")
}

func Sentemail(To string,Subject string,content string) {
	e := email.NewEmail()
	//设置发送方的邮箱
	e.From = "khaos服务器 <"+username+">"
	// 设置接收方的邮箱
	e.To = []string{To}
	//设置主题
	e.Subject = Subject
	//设置文件发送的内容
	e.Text = []byte(content)
	//设置服务器相关的配置
	err := e.Send(addr, smtp.PlainAuth("", username, password, host))
	if err != nil {
		log.Fatal(err)
	}
}
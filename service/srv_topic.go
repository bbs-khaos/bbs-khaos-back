package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func Topic_Create(Topic *model.Topic) error{
	return common.DB.Create(Topic).Error
}

func Topic_Delete(id string) error{
	var topic model.Topic
	return common.DB.Delete(&topic,id).Error
}

func Topic_ListAll(topics *[]model.Topic)  error{
	return common.DB.Find(&topics).Error
}

func Topic_GetCount() (uint32, error)  {
	var count uint32

	return count, common.DB.Table("topics").Where("topic_id >0 ").Count(&count).Error
}

func Topic_Update(topic *model.Topic) error {
	err := common.DB.Where("topic_id = ?", topic.Topic_id).First(topic).Error
	if err != nil {
		return err
	}
	return common.DB.Model(topic).Where("topic_id = ?", topic.Topic_id).Update("topic_logo", topic.Topic_logo).Error
}

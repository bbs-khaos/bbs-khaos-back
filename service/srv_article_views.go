package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func Article_view_Create(Article_view *model.Article_views) error {
	return common.DB.Create(&Article_view).Error
}

func Article_views_GetCount(Article_id string) (uint32, error) {
	var count uint32
	return count, common.DB.Table("article_views").Where("article_id = ?", Article_id).Count(&count).Error
}

func Article_views_ListSome(Article_views *[]model.Article_views, Article_id string, start uint32, amount uint32) error {
	common.AddView(common.AtoUin32(Article_id),"topic_article")
	return common.DB.Where("article_id = ?", Article_id).Offset(start).Limit(amount).Find(Article_views).Error
}

func Article_views_ListAll(Article_views *[]model.Article_views, Article_id string) error {
	common.AddView(common.AtoUin32(Article_id),"topic_article")
	return common.DB.Where("article_id = ?", Article_id).Find(Article_views).Error
}

func Article_view_Update(Article_view *model.Article_views) error {
	return common.DB.Model(Article_view).Where("view_id = ?", Article_view.View_id).Update("view_content", Article_view.View_content).Error
}

func Article_view_Delete(Article_view *model.Article_views) error {
	return common.DB.Where("view_id = ?", Article_view.View_id).Delete(&Article_view).Error
}

func Article_view_ListOne(view_id string,Article_views *model.Article_views) (error){

	return common.DB.Where("view_id = ?", view_id).Find(Article_views).Error
}
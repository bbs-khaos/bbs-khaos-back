package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func Topic_question_Create(Topic_question *model.Topic_questions) error{
	return common.DB.Create(Topic_question).Error
}
func Topic_question_Delete(Topic_question *model.Topic_questions) error {
	err := common.DB.Where("question_id = ? AND question_pub_user_id = ?", Topic_question.Question_id, Topic_question.Question_pub_user_id).First(Topic_question).Error
	if err != nil {
		return err
	}
	return common.DB.Where("question_id = ? AND question_pub_user_id = ?", Topic_question.Question_id, Topic_question.Question_pub_user_id).Delete(&Topic_question).Error
}
func Topic_question_ListAll(Topic_question *[]model.Topic_questions,topic_id string) (err error){
	common.AddView(common.AtoUin32(topic_id),"topic")
	return common.DB.Where("topic_id = ?", topic_id).Find(Topic_question).Error
}
func Topic_question_GetCount(Topic_id string) (uint32, error) {
	var count uint32
	return count, common.DB.Table("topic_questions").Where("topic_id = ?", Topic_id).Count(&count).Error
}
func Topic_question_Update(Topic_question *model.Topic_questions) error {
	p:= common.DB.Model(Topic_question).Where("question_id = ? AND question_pub_user_id = ?", Topic_question.Question_id, Topic_question.Question_pub_user_id).Update("question_content", Topic_question.Question_content).Error
	return p
}


func Topic_question_ListOne(question_id string,Topic_questions *model.Topic_questions) (error){

	return common.DB.Where("question_id = ?", question_id).Find(Topic_questions).Error
}

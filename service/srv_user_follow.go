package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func User_Detail_Create(User_detail *model.User_detail)error{	//创建新用户
	return common.DB.Create(User_detail).Error
}

func User_Detail(userId uint32, User_detail *model.User_detail) { //根据用户id获取用户的关注和发布 相关信息
	common.DB.Where(`user_id = ?`, userId).First(User_detail)
}

func User_Follow_Update(userId uint32,followList string) error {  //更新用户的关注信息
	return common.DB.Model(&model.User_detail{}).Where("User_id = ?",userId).Update("User_follows_id", followList).Error
}

func User_Follower_Update(userId uint32,followerList string) error {  //更新用户的被关注信息
	return common.DB.Model(&model.User_detail{}).Where("User_id = ?",userId).Update("User_followers_id", followerList).Error
}

func User_Question_Update(userId uint32,questionList string) error { //更新用户发布的问题id列表
	return common.DB.Model(&model.User_detail{}).Where("User_id = ?",userId).Update("User_questions_id", questionList).Error
}

func User_Answer_Update(userId uint32,answerList string) error { //更新用户发布的回答id列表
	return common.DB.Model(&model.User_detail{}).Where("User_id = ?",userId).Update("User_answers_id", answerList).Error
}

func User_Article_Update(userId uint32,articleList string) error { //更新用户发布的文章id列表
	return common.DB.Model(&model.User_detail{}).Where("User_id = ?",userId).Update("User_articles_id", articleList).Error
}

//待添加：获取所有用户（调试用）
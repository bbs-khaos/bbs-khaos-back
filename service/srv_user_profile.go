package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func User_profile_Login(User_profile *model.User_profile, email string) {
	common.DB.Where("user_email = ?", email).First(User_profile)
}

//func User_ChecktelInfo(User_profile *model.User_profile, telephone string) {
//	common.DB.Where("user_tel = ?", telephone).First(User_profile)
//}
func User_CheckemailInfo(User_profile *model.User_profile, telephone string) {
	common.DB.Where("user_email = ?", telephone).First(User_profile)
}

func User_profile_Create(User_profile *model.User_profile) {
	common.DB.Create(User_profile)
}

func User_detail_Create(User_detail *model.User_detail) {
	common.DB.Create(User_detail)
}

func User_profile_Delete(id string) error{
	var user model.User_profile
	return common.DB.Delete(&user,id).Error
}

func User_profile_Upgrade(user *model.User_profile) error {
	return common.DB.Model(user).Where("user_id = ?", user.User_id).Update("user_identity", user.User_identity).Error
}
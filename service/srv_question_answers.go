package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func Question_answer_Create(Question_answers *model.Question_answers) error {
	return common.DB.Create(&Question_answers).Error
}

func Question_answer_GetCount(Question_id string) (uint32, error) {
	var count uint32
	return count, common.DB.Table("question_answers").Where("question_id = ?", Question_id).Count(&count).Error
}

func Question_answer_ListSome(Question_answer *[]model.Question_answers, Question_id string, start uint32, amount uint32) error {
	common.AddView(common.AtoUin32(Question_id),"topic_question")
	return common.DB.Where("question_id = ?", Question_id).Offset(start).Limit(amount).Find(Question_answer).Error
}

func Question_answer_ListAll(Question_answer *[]model.Question_answers, Question_id string) error {
	common.AddView(common.AtoUin32(Question_id),"topic_question")
	return common.DB.Where("question_id = ?", Question_id).Find(Question_answer).Error
}

func Question_answer_Update(Question_answer *model.Question_answers) error {
	return common.DB.Model(Question_answer).Where("answer_id = ?", Question_answer.Answer_id, Question_answer.User_id).Update("answer_content", Question_answer.Answer_content).Error
}

func Question_answer_Delete(Question_answer *model.Question_answers) error {
	return common.DB.Where("answer_id = ?", Question_answer.Answer_id).Delete(&Question_answer).Error
}


func Question_answer_ListOne(answer_id string,Question_answers *model.Question_answers) (error){

	return common.DB.Where("answer_id = ?", answer_id).Find(Question_answers).Error
}


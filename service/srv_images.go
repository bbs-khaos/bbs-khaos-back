package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func UploadImage(img *model.Image) error { //上传图片信息到数据库
	return common.DB.Create(img).Error
}

func ImageInfo(imageInfo *model.Image, imgId string) error { //查询对应id文件的详细信息
	return common.DB.Where("id=?", imgId).First(imageInfo).Error
}

func DeleteImage(id string) error { //删除对应id的图片
	return common.DB.Delete(&model.Image{}, id).Error
}

func Image_ListAll(images *[]model.Image, userId string) error { //获取用户上传的所有图片信息
	return common.DB.Where("user_id = ?", userId).Find(images).Error
}

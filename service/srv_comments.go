package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func Comments_Create(Comment *model.Comments) error {
	return common.DB.Create(Comment).Error
}

func Comments_GetCount(at_id string, comment_type string) (uint32, error) {
	var count uint32
	return count, common.DB.Table("comments").Where("at_id = ? AND comment_type = ?", at_id, comment_type).Count(&count).Error
}

func Comments_ListSome(Comments *[]model.Comments, at_id string, comment_type string, start uint32, amount uint32) error {
	return common.DB.Where("at_id = ? AND comment_type = ?", at_id, comment_type).Offset(start).Limit(amount).Find(Comments).Error
}

func Comments_ListAll(Comments *[]model.Comments, at_id string, comment_type string) error {
	return common.DB.Where("at_id = ? AND comment_type = ?", at_id, comment_type).Find(Comments).Error
}

func Comments_Update(Comment *model.Comments) error {
	return common.DB.Model(Comment).Where("comment_id = ?", Comment.Comment_id).Update("comment_content", Comment.Comment_content).Error
}

func Comments_Delete(Comment *model.Comments) error {
	return common.DB.Where("comment_id = ?", Comment.Comment_id).Delete(Comment).Error
}

func Comments_ListOne(comment_id string,Comments *model.Comments) (error){

	return common.DB.Where("comment_id = ?", comment_id).Find(Comments).Error
}
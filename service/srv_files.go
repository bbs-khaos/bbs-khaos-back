package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func UploadFile(file *model.File) error { //上传文件
	return common.DB.Create(file).Error
}

func FileInfo(fileInfo *model.File, fileId string) error { //查询对应id文件的详细信息
	return common.DB.Where("id = ?", fileId).First(fileInfo).Error
}

func DeleteFile(id string) error { //删除对应id的文件
	return common.DB.Delete(&model.File{}, id).Error
}

func File_ListAll(files *[]model.File, userId string) error { //获取用户上传的所有文件信息
	return common.DB.Where("user_id = ?", userId).Find(files).Error
}

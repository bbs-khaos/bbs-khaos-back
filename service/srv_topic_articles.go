package service

import (
	"khao.fun/shizhan/common"
	"khao.fun/shizhan/model"
)

func Topic_articles_Create(Topic_articles *model.Topic_articles) error{
	return common.DB.Create(Topic_articles).Error
}
func Topic_articles_Delete(Topic_articles *model.Topic_articles) error {
	err := common.DB.Where("article_id = ?", Topic_articles.Article_id).First(Topic_articles).Error
	if err != nil {
		return err
	}
	return common.DB.Where("article_id = ?", Topic_articles.Article_id).Delete(&Topic_articles).Error
}

func Topic_articles_ListAll(Topic_articles *[]model.Topic_articles, Topic_id string)  error{
	common.AddView(common.AtoUin32(Topic_id),"topic")
	return common.DB.Where("topic_id = ?", Topic_id).Find(Topic_articles).Error
}

func Topic_articles_GetCount(Topic_id string) (uint32, error) {
	var count uint32
	return count, common.DB.Table("topic_articles").Where("topic_id = ?", Topic_id).Count(&count).Error
}

func Topic_articles_Update(Topic_articles *model.Topic_articles) error {
	return common.DB.Model(Topic_articles).Where("article_id = ?", Topic_articles.Article_id).Update("article_content", Topic_articles.Article_content).Error
}

func Topic_articles_ListOne(article_id string,Topic_articles *model.Topic_articles) (error){

	return common.DB.Where("article_id = ?", article_id).Find(Topic_articles).Error
}
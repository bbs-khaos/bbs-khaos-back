package dto

import (
	"time"

	"khao.fun/shizhan/model"
)

type ImageDto struct {
	Id           int       `json:"image_id"`
	Type         string    `json:"image_type"` // 类型 (avatar,topic)
	UserId       uint32    `json:"user_id"`    // 上传的用户 id
	ImagePath    string    `json:"image_path"` // 图片路径
	Created_date time.Time `json:"created_date"`
	Updated_date time.Time `json:"updated_date"`
}

func Dto_image(image model.Image) ImageDto {
	return ImageDto{
		Id:           image.Id,
		Type:         image.Type,
		UserId:       image.UserId,
		ImagePath:    image.ImagePath,
		Created_date: image.Created_date,
		Updated_date: image.Updated_date,
	}
}

package dto

import (
	"khao.fun/shizhan/common"
	"time"

	"khao.fun/shizhan/model"
)

type Topic_articlesDto struct {
	Topic_id                uint32    `json:"topic_id"`
	Article_id              uint32    `json:"article_id"`
	Article_pub_user_id     uint32    `json:"article_pub_user_id"`
	Article_title           string    `json:"article_title"`
	Article_content         string    `json:"article_content"`
	Article_comments_amount uint32    `json:"article_comments_amount"`
	View                 	uint64    `json:"view"`
	Created_date            time.Time `json:"created_date"`
	Updated_date            time.Time `json:"updated_date"`
}

func Dto_Topic_articles(Topic_articles model.Topic_articles) Topic_articlesDto {
	return Topic_articlesDto{
		Topic_id:                Topic_articles.Topic_id,
		Article_id:              Topic_articles.Article_id,
		Article_pub_user_id:     Topic_articles.Article_pub_user_id,
		Article_title:           Topic_articles.Article_title,
		Article_content:         Topic_articles.Article_content,
		Article_comments_amount: Topic_articles.Article_comments_amount,
		View:					 common.View(Topic_articles.Article_id,"topic_article"),
		Created_date:            Topic_articles.Created_date,
		Updated_date:            Topic_articles.Updated_date,
	}
}

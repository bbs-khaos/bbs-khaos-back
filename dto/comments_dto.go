package dto

import (
	"time"

	"khao.fun/shizhan/model"
)

type CommentsDto struct {
	Comment_id      uint32    `json:"comment_id"`
	Comment_type    uint8     `json:"comment_type"`
	At_id           uint32    `json:"at_id"`
	From_user_id    uint32    `json:"from_user_id"`
	To_user_id      uint32    `json:"to_user_id"`
	Comment_content string    `json:"comment_content"`
	Comment_like    uint32    `json:"comment_like"`
	Comment_unlike  uint32    `json:"comment_unlike"`
	Created_date    time.Time `json:"created_date"`
	Updated_date    time.Time `json:"updated_date"`
}

func Dto_Comments(Comments model.Comments) CommentsDto {
	return CommentsDto{
		Comment_id:      Comments.Comment_id,
		Comment_type:    Comments.Comment_type,
		At_id:           Comments.At_id,
		From_user_id:    Comments.From_user_id,
		To_user_id:      Comments.To_user_id,
		Comment_content: Comments.Comment_content,
		Comment_like:    Comments.Comment_like,
		Comment_unlike:  Comments.Comment_unlike,
		Created_date:    Comments.Created_date,
		Updated_date:    Comments.Updated_date,
	}
}

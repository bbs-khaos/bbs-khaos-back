package dto

import (
	"khao.fun/shizhan/common"
	"time"

	"khao.fun/shizhan/model"
)

type Topic_questionsDto struct {
	Topic_id                uint32    `json:"topic_id"`
	Question_id             uint32    `json:"question_id"`
	Question_pub_user_id    uint32    `json:"question_pub_user_id"`
	Question_title          string    `json:"question_title"`
	Question_content        string    `json:"question_content"`
	Question_answers_amount uint32    `json:"question_answers_amount"`
	View                 	uint64    `json:"view"`
	Created_date            time.Time `json:"created_date"`
	Updated_date            time.Time `json:"updated_date"`
}

func Dto_Topic_questions(Topic_questions model.Topic_questions) Topic_questionsDto {
	return Topic_questionsDto{
		Topic_id:                Topic_questions.Topic_id,
		Question_id:             Topic_questions.Question_id,
		Question_pub_user_id:    Topic_questions.Question_pub_user_id,
		Question_title:          Topic_questions.Question_title,
		Question_content:        Topic_questions.Question_content,
		Question_answers_amount: Topic_questions.Question_answers_amount,
		View:					 common.View(Topic_questions.Question_id,"topic_question"),
		Created_date:            Topic_questions.Created_date,
		Updated_date:            Topic_questions.Updated_date,
	}
}

package dto

import "khao.fun/shizhan/model"

type User_profileDto struct {
	User_id   		  uint32 `json:"user_id"`
	User_name 		  string `json:"user_name"`
	User_email        string `json:"user_email"`
	User_sex  		  string `json:"user_sex"`
	User_tel  		  string `json:"user_tel"`
	// User_pwd          string `json:"user_pwd"`
	User_logo         string `json:"user_logo"`
	User_province     string `json:"user_province"`
	User_city         string `json:"user_city"`
	User_identity     uint8  `json:"user_identity"`
	User_introduction string `json:"user_introduction"`
}

func Dto_User_profile(User_profile model.User_profile) User_profileDto {
	return User_profileDto{
		User_id:   		   User_profile.User_id,
		User_name: 		   User_profile.User_name,
		User_email:        User_profile.User_email,
		User_sex:  		   User_profile.User_sex,
		User_tel:  		   User_profile.User_tel,
		// User_pwd:          User_profile.User_pwd,
		User_province:     User_profile.User_province,
		User_city:         User_profile.User_city,
		User_identity:     User_profile.User_identity,
		User_introduction: User_profile.User_introduction,
	}
}

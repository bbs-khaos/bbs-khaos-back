package dto

import (
	"time"

	"khao.fun/shizhan/model"
)

type Question_answersDto struct {
	Question_id    uint32    `json:"question_id"`
	Answer_id      uint32    `json:"answer_id"`
	User_id        uint32    `json:"user_id"`
	Answer_content string    `json:"answer_content"`
	Answer_like    uint32    `json:"answer_like"`
	Answer_unlike  uint32    `json:"answer_unlike"`
	Created_date   time.Time `json:"created_date"`
	Updated_date   time.Time `json:"updated_date"`
}

func Dto_Question_answers(Question_answers model.Question_answers) Question_answersDto {
	return Question_answersDto{
		Question_id:    Question_answers.Question_id,
		Answer_id:      Question_answers.Answer_id,
		User_id:        Question_answers.User_id,
		Answer_content: Question_answers.Answer_content,
		Answer_like:    Question_answers.Answer_like,
		Answer_unlike:  Question_answers.Answer_unlike,
		Created_date:   Question_answers.Created_date,
		Updated_date:   Question_answers.Updated_date,
	}
}

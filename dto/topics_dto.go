package dto

import (
	"khao.fun/shizhan/common"
	"time"

	"khao.fun/shizhan/model"
)

type TopicDto struct {
	Topic_id                   uint32    `json:"topic_id"`
	Topic_logo                 string    `json:"topic_logo"`
	View                 	   uint64    `json:"view"`
	Topic_title                string    `json:"topic_title"`
	Topic_introduction         string    `json:"topic_introduction"`
	Topic_subscription_user_id string    `json:"topic_subscription_user_id"`
	Created_date               time.Time `json:"created_date"`
	Updated_date               time.Time `json:"updated_date"`
}

func Dto_Topic(Topic model.Topic) TopicDto {
	return TopicDto{
		Topic_id:                   Topic.Topic_id,
		Topic_logo:                 Topic.Topic_logo,
		Topic_title:                Topic.Topic_title,
		View:						common.View(Topic.Topic_id,"topic"),
		Topic_introduction:         Topic.Topic_introduction,
		Topic_subscription_user_id: Topic.Topic_subscription_user_id,
		Created_date:               Topic.Created_date,
		Updated_date:               Topic.Updated_date,
	}
}

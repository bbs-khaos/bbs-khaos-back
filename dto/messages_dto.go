package dto

import (
	"time"

	"khao.fun/shizhan/model"
)

type MessagesDto struct {
	Message_id      uint32    `json:"message_id"`
	Belong_user_id  uint32    `json:"belong_user_id"`
	Type            uint8     `json:"type"`
	Subject_user_id uint32    `json:"subject_user_id"`
	Module_id       uint32    `json:"module_id"`
	Created_date    time.Time `json:"created_date"`
	Updated_date    time.Time `json:"updated_date"`
}

func Dto_Message(Messages model.Messages) MessagesDto {
	return MessagesDto{
		Message_id:      Messages.Message_id,
		Belong_user_id:  Messages.Belong_user_id,
		Subject_user_id: Messages.Subject_user_id,
		Type:            Messages.Type,
		Module_id:       Messages.Module_id,
		Created_date:    Messages.Created_date,
		Updated_date:    Messages.Updated_date,
	}
}

package dto

import (
	"time"

	"khao.fun/shizhan/model"
)

type Article_viewsDto struct {
	Article_id   uint32    `json:"article_id"`
	View_id      uint32    `json:"view_id"`
	User_id      uint32    `json:"user_id"`
	View_content string    `json:"view_content"`
	View_like    uint32    `json:"view_like"`
	View_unlike  uint32    `json:"view_unlike"`
	Created_date time.Time `json:"created_date"`
	Updated_date time.Time `json:"updated_date"`
}

func Dto_Article_views(Article_views model.Article_views) Article_viewsDto {
	return Article_viewsDto{
		Article_id:   Article_views.Article_id,
		View_id:      Article_views.View_id,
		User_id:      Article_views.User_id,
		View_content: Article_views.View_content,
		View_like:    Article_views.View_like,
		View_unlike:  Article_views.View_unlike,
		Created_date: Article_views.Created_date,
		Updated_date: Article_views.Updated_date,
	}
}

package dto

import (
	"time"

	"khao.fun/shizhan/model"
)

type FileDto struct {
	Id           int       `json:"file_id"`   //文件id
	UserId       uint32    `json:"user_id"`   //上传的用户 id
	Filename     string    `json:"file_name"` //文件名
	FilePath     string    `json:"file_path"` //文件路径
	Created_date time.Time `json:"created_date"`
	Updated_date time.Time `json:"updated_date"`
}

func Dto_File(file model.File) FileDto {
	return FileDto{
		Id:           file.Id,
		UserId:       file.UserId,
		Filename:     file.Filename,
		FilePath:     file.FilePath,
		Created_date: file.Created_date,
		Updated_date: file.Updated_date,
	}
}

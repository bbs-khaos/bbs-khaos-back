package dto

import (
	"time"

	"khao.fun/shizhan/model"
)

type User_detailDto struct {
	User_id           uint32    `json:"user_id"`
	User_follows_id   string    `json:"user_follows_id"`
	User_followers_id string    `json:"user_followers_id"`
	User_questions_id string    `json:"user_questions_id"`
	User_answers_id   string    `json:"user_answers_id"`
	User_articles_id  string    `json:"user_articles_id"`
	Created_date      time.Time `json:"created_date"`
	Updated_date      time.Time `json:"updated_date"`
}

func Dto_User_detail(User_detail model.User_detail) User_detailDto {
	return User_detailDto{
		User_id:           User_detail.User_id,
		User_follows_id:   User_detail.User_follows_id,
		User_followers_id: User_detail.User_followers_id,
		User_questions_id: User_detail.User_questions_id,
		User_answers_id:   User_detail.User_answers_id,
		User_articles_id:  User_detail.User_articles_id,
		Created_date:      User_detail.Created_date,
		Updated_date:      User_detail.Updated_date,
	}
}
